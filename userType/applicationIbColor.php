<?php

class applicationIbColor {

	function addHeaders() {

		global $APPLICATION;
		$APPLICATION->AddHeadScript('/bitrix/js/main/jquery/jquery-1.8.3.min.js');
		$APPLICATION->SetAdditionalCSS('/local/php_interface/userType/js/colorpicker.css');
		$APPLICATION->AddHeadScript('/local/php_interface/userType/js/colorpicker.js');
		$APPLICATION->AddHeadScript('/local/php_interface/userType/js/script.js');
	}

	function GetUserTypeDescription() {
		return array(
			"USER_TYPE_ID" => "color",
			"CLASS_NAME" => __CLASS__,
			"DESCRIPTION" => "Цвет [obgonay.ru]",
			"BASE_TYPE" => "int",
		);
	}

	function GetIBlockPropertyDescription() {
		return array(
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "color",
			"DESCRIPTION" => "Цвет [obgonay.ru]",
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
		);
	}

	function getViewHTML($name, $value) {
		return "<div><div style='background-color: #" . $value . "; width: 20px;height: 20px;'></div></div>";
	}

	function getEditHTML($name, $value, $is_ajax = false) {
		$key = sha1($name);
		$string = '';
		self::addHeaders();
		$string.='<div id="colorSelector1"><div style="background-color: #' . $value . '"></div></div>';
		$string.='<input type="hidden" id="color" name="' . $name . '" value="' . $value . '"/>';

		return $string;
	}

	function getAdminEditHTML($name, $value, $is_ajax = false) {
		$string = '';
		self::addHeaders();
		$string.='<div class="colorSelector3"><div style="background-color: #' . $value . '"></div></div>';
		$string.='<input type="hidden" id="color" name="' . $name . '" value="' . $value . '"/>';

		return $string;
	}

	function GetEditFormHTML($arUserField, $arHtmlControl) {
		return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
	}

	function GetAdminListEditHTML($arUserField, $arHtmlControl) {
		return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
	}

	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
		return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
		/* return $strHTMLControlName['MODE'] == 'FORM_FILL'
		  ? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false)
		  : self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE'])
		  ; */
		$field = self::getAdminEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false);
		if ($arProperty["WITH_DESCRIPTION"] == "Y") {
			$field.='&nbsp;Описание <input type="text" size="20" name="' . $strHTMLControlName["DESCRIPTION"] . '" value="' . htmlspecialcharsbx($value["DESCRIPTION"]) . '">';
		}
		return $field;
	}

}

?>