<?php

class applicationCountry extends CUserTypeInteger {

    function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "applicationCountry",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "Страна",
            "BASE_TYPE" => "int",
        );
    }

    function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "Страна",
            "DESCRIPTION" => "Страна",
        );
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl){
        pre($arHtmlControl["VALUE"]);
        return GetCountryByID($arHtmlControl["VALUE"]);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)  {
        $key = sha1($name);
        $string = '';

        pre($arUserField, $arHtmlControl);

        $arCountries = GetCountryArray();
        $string .= <<<HTML
        <select name="{$arUserField['EDIT_FORM_LABEL']}" style="width: 250px;">
            <option>(не выбрано)</option>
HTML;
        foreach ($arCountries['reference'] as $k => $v) {
            $isSelected = $arCountries['reference_id'][$k] == $value ? 'selected="selected"' : '';
            $string .= <<<HTML
                <option value="{$arCountries['reference_id'][$k]}" {$isSelected}>{$v}</option>
HTML;
        }
        $string .= '</select>';
        return $string;
    }


}
?>